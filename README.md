GNU postical
========

Post notice to any StatusNet or GNU Social based site from command line.

**Usage**

**send notice**

./post [-i image] [-s source] [-u user] [-p password] [-t latitude -g longitude] [-r in_reply_to_status_id] [-x proxyhost:port] notice

**repost notice**

./repost [-s source] [-u user] [-p password]  ID

**read timeline**

 ./gettimeline [-c <command>]
where <command> is one of:

*  p	Returns the 20 most recent statuses from non-protected users who have set a custom user icon.

*  h	-u user [-p password] [-s since_id] [-m max_id] [-o count] [-g page] 
	Returns the 20 most recent statuses, including retweets, posted by the authenticating user and that user's friends.

*  f	-u user [-p password] [-s since_id] [-m max_id] [-o count] [-g page] 
	Returns the 20 most recent statuses posted by the authenticating user and that user's friends.

*  u	-u user [-p password] [-i id] [-s since_id] [-m max_id] [-o count] [-g page] 
	Returns the 20 most recent statuses posted from the authenticating user. It's also possible to request another user's timeline via the id parameter.

*  r	-u user [-p password] [-i id] [-s since_id] [-m max_id] [-o count] [-g page] 
	Returns the 20 most recent mentions (status containing @username) for the authenticating user.
